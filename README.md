# Michael Sammels

## Welcome to my GitLab Profile

I'm a passionate software developer with a love for elegant code and problem-solving. This is where you can get to
know more about my skills, projects, and interests.

---

## Connect With Me

[![LinkedIn Badge](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://linkedin.com/in/msammels)
[![Website Badge](https://img.shields.io/badge/Website-3b5998?style=for-the-badge&logo=safari&logoColor=white)](https://sammels.ai)

---

## Technology Stack

- **Languages**

  ![](https://skillicons.dev/icons?i=bash,c,cpp,css,html,java,js,md,swift&theme=light)

- **Frameworks & Tools**

  ![](https://skillicons.dev/icons?i=bootstrap,docker,gitlab,linux&theme=light)

- **Databases**

  ![](https://skillicons.dev/icons?i=mongodb,mysql,postgres,sqlite&theme=light)

- **IDES & Interfaces**

  ![](https://skillicons.dev/icons?i=eclipse,emacs&theme=light)

---

## About Me

- 🔤 I have a keen appreciation for the 80-character limit in code..
- 📫 Reach me at ***michael@sammels.ai***
- ⚡️ Fun fact: While you close your eyes, this fact is decrypted 😂

---

## Projects & Contributions

### <span style="display: inline-block; vertical-align: middle;"><img src="https://gitlab.com/uploads/-/system/project/avatar/44992187/adobe-photoshop-1990-eye-logo-9DBCF3C58C-seeklogo.com.png?width=64"></span> [Adobe Photoshop][adobe-photoshop]

Contributed to the preservation of computing history by hosting the historical source code of Adobe Photoshop. This
repository provides valuable insights into the evolution of one of the world's most iconic image editing software.

---

### <span style="display: inline-block; vertical-align: middle;"><img src="https://gitlab.com/uploads/-/system/project/avatar/44092534/microsoft-office-word.png?width=64"></span> [Microsoft Word for Windows][microsoft-word-for-windows]

Participated in preserving the legacy of software by hosting the historical source code of Microsoft Word for
Windows. This repository showcases the development of a widely used word processing application.

---

### <span style="display: inline-block; vertical-align: middle;"><img src="assets/lochaber.jpeg" alt="Lochaber" width="64"></span> Lochaber

Actively developing a 64-bit operating system known as Lochaber. This project aims to demonstrate expertise in
low-level systems programming and design, with the goal ultimately to be creating a 64-bit environment for the
modern world.

---

### <span style="display: inline-block; vertical-align: middle;"><img src="assets/placeholder.png" alt="Attribute Pro" width="64"></span> Attribute Pro

Streamlined Multiplatform application for Apple operating systems, allowing users to view and/or modify file attributes.

---

## Current Focus

I'm currently working on refining my skills in assembly language and exploring the realms of progressing my custom
operating system to the next level. Exciting things are in the pipeline! Although, how long exactly this pipeline
is... no one knows 😂

---

### Feel free to explore around, have fun, and take care!

[adobe-photoshop]: https://gitlab.com/msammels/Adobe-Photoshop
[microsoft-word-for-windows]: https://gitlab.com/msammels/Microsoft-Word-for-Windows
